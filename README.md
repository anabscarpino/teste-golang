# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Programa criado para teste técnico.
Objetivo: ler um arquivo csv/txt e persistir os dados em um banco relacional.

### How do I get set up? ###

Linguagem: Go
Banco: PostGreSQL

1 - Utilizando o pgAdmin como interface, criar um banco de nome padrão "postgres" também com senha padrão "postgres" e abrir o arquivo "scripts.sql" e executar. Após a execução, será criada a tabela "extrato"
2 - O código fonte está presente no arquivo "program.go", para executá-lo basta executar o comando "go run .\program.go"
3 - O console lhe pedirá o path do arquivo a ser lido (ou o nome, caso o arquivo esteja na mesma pasta do projeto), como por exemplo "base_teste.txt".
4 - Após digitar o nome do arquivo/path, o programa iniciará.
5 - Quando finalizar a importação dos dados para o banco, o programa terminará sem erros.
