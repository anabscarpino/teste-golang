package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"os"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "postgres"
)

func main() {
	sqlConexao := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	bancoPostgres, err := sql.Open("postgres", sqlConexao)

	if err != nil {
		fmt.Println("Erro ao abrir conexão com o banco:", err)
	}

	nomeArquivo := leNomeArquivo()

	if !strings.HasSuffix(nomeArquivo, "txt") && !strings.HasSuffix(nomeArquivo, ".csv") {
		fmt.Println("Erro! Tipo de arquivo não suportado.")
		os.Exit(-1)
	}

	arquivo, err := os.Open(nomeArquivo)
	if err != nil {
		fmt.Println("Erro ao abrir arquivo", nomeArquivo, ":", err)
	}

	scanner := bufio.NewScanner(arquivo)
	scanner.Split(bufio.ScanLines)

	cabecalho := true

	for scanner.Scan() {
		var colunas []string

		if strings.HasSuffix(nomeArquivo, "txt") {
			colunas = strings.Fields(scanner.Text())

		} else {
			colunas = strings.Split(scanner.Text(), ";")
		}
		// lê linha a linha do arquivo, pulando apenas o cabeçalho com os nomes das colunas
		if !cabecalho {
			cpf, priv, incompleto, dtUltimaCompra, ticketMedio, ticketUltimaCompra, ljFrequente, ljUltimaCompra, erro := leRegistro(colunas)
			insert := criaComandoInsert(cpf, priv, incompleto, dtUltimaCompra, ticketMedio, ticketUltimaCompra, ljFrequente, ljUltimaCompra)

			if erro {
				fmt.Println("Erro no comando:", insert.String(), "pois CPF ou CNPJ está inválido")
				continue
			}

			_, err := bancoPostgres.Exec(insert.String())
			if err != nil {
				fmt.Println("Erro ao executar o comando:", insert.String(), "Erro:", err)
			}
		}
		cabecalho = false
	}

	defer bancoPostgres.Close()
	defer arquivo.Close()
}

func verdadeiroFalso(s string) bool {
	return s == "1"
}

func formataCpfCnpj(s string) (string, bool) {
	ehValido := true
	if s != "NULL" {
		novaString := "'"
		s = strings.TrimSpace(s)
		s = strings.ReplaceAll(s, ".", "")
		s = strings.ReplaceAll(s, "-", "")
		s = strings.ReplaceAll(s, "/", "")
		novaString += s + "'"

		if len(novaString) == 11 {
			ehValido = CpfValido(novaString)
		} else if len(novaString) == 14 {
			ehValido = CnpjValido(novaString)
		} else {
			ehValido = false
		}
		return novaString, ehValido
	}

	return strings.ToLower(s), ehValido
}

func formataDecimal(s string) string {
	s = strings.TrimSpace(s)
	s = strings.ReplaceAll(s, ",", ".")
	return strings.ToLower(s)
}

func formataData(s string) string {
	if s != "NULL" {
		novaString := "'"
		s = strings.TrimSpace(s)
		novaString += s + "'"
		return novaString
	}

	return strings.ToLower(s)
}

func leNomeArquivo() string {
	var nomeArquivo string
	fmt.Println("Nome do arquivo:")
	fmt.Scan(&nomeArquivo)
	fmt.Println("Inserindo registros do arquivo", nomeArquivo, "no banco...")
	return nomeArquivo
}

func leRegistro(s []string) (string, bool, bool, string, string, string, string, string, bool) {
	cpf, valido1 := formataCpfCnpj(s[0])
	priv := verdadeiroFalso(s[1])
	incompleto := verdadeiroFalso(s[2])
	dtUltimaCompra := formataData(s[3])
	ticketMedio := formataDecimal(s[4])
	ticketUltimaCompra := formataDecimal(s[5])
	ljFrequente, valido2 := formataCpfCnpj(s[6])
	ljUltimaCompra, valido3 := formataCpfCnpj(s[7])
	erro := false
	if valido1 && valido2 && valido3 {
		erro = false
	}
	return cpf, priv, incompleto, dtUltimaCompra, ticketMedio, ticketUltimaCompra, ljFrequente, ljUltimaCompra, erro
}

func criaComandoInsert(cpf string, priv bool, incompleto bool, dtUltimaCompra string, ticketMedio string, ticketUltimaCompra string, ljFrequente string, ljUltimaCompra string) strings.Builder {
	var comando strings.Builder
	comando.WriteString("insert into extrato (cpf_cnpj, private, incompleto, dt_ultima_compra, ticket_medio, ticket_ultima_compra, lj_frequente, lj_ultima_compra) values(")
	comando.WriteString(cpf)
	comando.WriteString(",")
	comando.WriteString(strconv.FormatBool(priv))
	comando.WriteString(",")
	comando.WriteString(strconv.FormatBool(incompleto))
	comando.WriteString(",")
	comando.WriteString(dtUltimaCompra)
	comando.WriteString(",")
	comando.WriteString(ticketMedio)
	comando.WriteString(",")
	comando.WriteString(ticketUltimaCompra)
	comando.WriteString(",")
	comando.WriteString(ljFrequente)
	comando.WriteString(",")
	comando.WriteString(ljUltimaCompra)
	comando.WriteString(");")

	return comando
}

func CpfValido(cpf string) bool {
	digito1 := 0
	digito2 := 0
	for i, j := 0, 10; i < len(cpf)-2; i, j = i+1, j-1 {
		digito1 = digito1 + (int(cpf[i])-48)*j
	}
	digito1 = digito1 % 11
	if digito1 < 2 {
		digito1 = 0
	} else {
		digito1 = 11 - digito1
	}

	if int(cpf[9])-48 != digito1 {
		return true
	} else {
		for i, j := 0, 11; i < len(cpf)-1; i, j = i+1, j-1 {
			digito2 = digito2 + (int(cpf[i]-48))*j
		}
		digito2 = digito2 % 11
		if digito2 < 2 {
			digito2 = 0
		} else {
			digito2 = 11 - digito2
		}

		if int(cpf[10])-48 != digito2 {
			return true
		}
	}
	return false
}

func CnpjValido(cnpj string) bool {
	multiplicador1 := [12]int{5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2}
	multiplicador2 := [13]int{6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2}

	tempCnpj := cnpj[:12]
	soma := 0

	for i := 0; i < 12; i++ {
		soma = soma + int(tempCnpj[i])*multiplicador1[i]
	}

	resto := soma % 11

	if resto < 2 {
		resto = 2
	} else {
		resto = 11 - resto
	}

	digito := string(resto)

	tempCnpj = tempCnpj + digito
	soma = 0
	for i := 0; i < 13; i++ {
		soma = soma + int(tempCnpj[i])*multiplicador2[i]
	}

	resto = soma % 11
	if resto < 2 {
		resto = 0
	} else {
		resto = 11 - resto
	}

	digito = digito + string(resto)

	return string(cnpj[13]) == digito
}
